# Miniproject9: Streamlit Chatbot with Hugging Face Model

## Introduction
This project showcases a web-based chatbot application built with Streamlit and powered by an open-source Large Language Model (LLM) from Hugging Face. I choose gpt2 model: https://huggingface.co/openai-community/gpt2. It's designed to provide users with an interactive chat experience, showcasing the capabilities of modern NLP technologies.

## Setup and Installation
- Ensure Python 3.6+ is installed.
- Clone this repository.
- Install required dependencies:
  ```bash
  pip install -r requirements.txt
    ```
## Steps

1. Create a .py file. (I created named app.py)
2. Create a Streamlit App account.
3. Edit your .py file based on the model you chosen from HuggingFace and design your website in .py file as well.
4. Run the command:
```
streamlit run <YOUR .py file name>
```

You will see the information shown below:
![original image](https://cdn.mathpix.com/snip/images/nkeqEGt0AhvBtLlgd8Ol2pJqAyFTsNmpXi3y-_zCitc.original.fullsize.png)

![original image](https://cdn.mathpix.com/snip/images/bOT2-RdLv1aJn5R8eqTz_0wnC7QOnImMeUtyyLcSqqY.original.fullsize.png)

Now you are able to view your website locally by Local URL.

5. Finally, if we want to make our website public, we can deploy it to Streamlit App. You can follow the instructions in Streamlit App. It's very easy to deploy when you have a Github account and link it to Streamlit App.

After that, you can get a public website link like:
https://app-ika9axeblxhbdgsawfy6rg.streamlit.app/


## Screenshot About My website
![original image](https://cdn.mathpix.com/snip/images/nIeX1zCXVnLBcVnp2GRozL3V-5ri2Hp8DubvKMF0HNU.original.fullsize.png)

The user inputs their own text into a field and can specify the maximum length of the AI-generated response via a slider, as well as the number of responses they wish to receive. After inputting their text and preferences, the user can press 'Submit' to receive the AI's continuation of their text. This app is designed to showcase conversational AI's capabilities in extending and playing with user-submitted prompts.
